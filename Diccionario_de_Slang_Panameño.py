import sqlite3
NOMBRE_BASE_DE_DATOS = "diccionarioSlangPanameño.db"


def obtener_conexion():
    return sqlite3.connect(NOMBRE_BASE_DE_DATOS)


def crear_tablas():
    tablas = [ """CREATE TABLE IF NOT EXISTS diccionario(id INTEGER PRIMARY KEY AUTOINCREMENT,palabra_frase TEXT NOT NULL,significado TEXT NOT NULL);"""]
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    for tabla in tablas:
        cursor.execute(tabla)


def principal():
    crear_tablas()
    MENU = """
    \t a) Agregar nueva palabra o frase

    \n\t b) Editar palabra o frase existente(s)

    \n\t c) Eliminar palabra o frase existente(s)

    \n\t d) Ver listado de datos

    \n\t e) Buscar significado de una palabra o frase

    \n\t f) Salir 

\n\t  Escoja una opción: """
    eleccion = ""
    while eleccion != "f":
        
        eleccion = input(MENU)
        if eleccion == "a":
            palabra_frase = input("\t\36¿Qué te gutaría agregar? ")#Add and verify if the input word exists or not
           
            posible_significado = buscar_significado_palabra(palabra_frase)
            if posible_significado:
                print(f"\t\36La palabra '{palabra_frase}' ya existe")#if our input word exists it will give us back an error
            else:
                significado = input("\t\36Ingresa el significado: ")
                agregar_palabra(palabra_frase, significado)
                print("\t\36Palabra agregada")
        if eleccion == "b":
            palabra_frase = input("\t\36¿Qué palabra o frase quieres editar? ")#To edit and save we will use nuevo_significado whenever we want
            nuevo_significado = input("\t\36Ingresa el nuevo significado: ")
            editar_palabra_frase(palabra_frase, nuevo_significado)
            print("\t\36Has actualizado con éxito")
        if eleccion == "c":
            palabra_frase = input("\t\36¿Qué palabra o frase quieres eliminar? ")
            eliminar_palabra(palabra_frase)
        if eleccion == "d":
            palabras = obtener_palabras()
            print("\t\36Aquí tienes las palabras y/o frases existentes")
            for palabra in palabras:
                print("\n\t",palabra[0])
        if eleccion == "e":
            palabra_frase = input(
                "\t\36¿De qué te gustaría saber el significado? ")
            significado = buscar_significado_palabra(palabra_frase)
            if significado:
                print(f"\tEl significado de '{palabra_frase}' es:{significado[0]}")
            else:
                print(f"\t\36Palabra '{palabra_frase}' no encontrada")
                


def agregar_palabra(palabra_frase, significado):
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    sentencia = "INSERT INTO diccionario(palabra_frase, significado) VALUES (?, ?)"
    cursor.execute(sentencia, [palabra_frase, significado])
    conexion.commit()


def editar_palabra_frase(palabra_frase, nuevo_significado):
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    sentencia = "UPDATE diccionario SET significado = ? WHERE palabra_frase = ?"
    cursor.execute(sentencia, [nuevo_significado, palabra_frase])
    conexion.commit()


def eliminar_palabra(palabra_frase):
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    sentencia = "DELETE FROM diccionario WHERE palabra_frase = ?"
    cursor.execute(sentencia, [palabra_frase])
    conexion.commit()


def obtener_palabras():
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    consulta = "SELECT palabra_frase FROM diccionario"
    cursor.execute(consulta)
    return cursor.fetchall()


def buscar_significado_palabra(palabra_frase):
    conexion = obtener_conexion()
    cursor = conexion.cursor()
    consulta = "SELECT significado FROM diccionario WHERE palabra_frase = ?"
    cursor.execute(consulta, [palabra_frase])
    return cursor.fetchone()


if __name__ == '__main__':
    principal()